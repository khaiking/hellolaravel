<?php

echo "HELLO ALL";
die;

const STEP_RANGE = 500000;

$id = $_GET["id"] ?? "";
$token = $_GET["tk"] ?? "";
$key = $_GET["k"] ?? "";

if ($id) {
    function downloadFile($id, $key, $token, $start = 0)
    {
        $end = $start + STEP_RANGE - 1;
        $curl = curl_init();
        $url = "https://www.googleapis.com/drive/v3/files/$id?alt=media";
        $headers = [
            "Range: bytes=$start-$end",
        ];
        if ($token) {
            $headers[] = "Authorization: Bearer $token";
        } else {
            $url .= "&key=$key";
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($http_code == 416) {
            // out byte ranges
            return null;
        }
        // echo json_encode($response_headers);
        // die;

        return $response;
    }

    $start = 0;
    do {
        $response = downloadFile($id, $key, $token, $start);

        if ($start == 0) {
            header("Content-Type: video/mp4");
            header('Content-Disposition: attachment; filename="test.mp4"');
        }

        $start += STEP_RANGE;
        echo $response;
    } while ($response);


    die;
}

header("X-Debug: NotFound", true, 404);
die;

# Version: 1.0.0.3